const execSync = require('child_process').execSync;
const path = require('path');
const {readPackageJsonFile} = require('./utils');

const cssPackageJsonFile = path.resolve(__dirname, '../packages/styles/package.json');
const jsPackageJsonFile = path.resolve(__dirname, '../package.json');

let jsret, cssret;
try {
  console.log('正在检查 npm 包版本信息...');
  jsret = execSync(`npm view lanaya-ui version`, {encoding: 'utf-8'});
  cssret = execSync(`npm view lanaya-ui-css version`, {encoding: 'utf-8'});
} catch (e) {
  console.log('获取 lanaya-ui 版本信息出错了...');
  process.exit(1);
}

let jsVersion = jsret.split('\n')[0];
let cssVersion = cssret.split('\n')[0];

if (jsVersion && cssVersion) {
  console.log(`npm 上 lanaya-ui 的最新版本为： ${jsVersion}`);
  console.log(`npm 上 lanaya-ui-css 的最新版本为： ${cssVersion}`);
  console.log('读取本地包版本信息...');
  Promise.all([readPackageJsonFile(jsPackageJsonFile), readPackageJsonFile(cssPackageJsonFile)]).then(ret => {
    let cssJson = {};
    let jsJson = {};
    try {
      jsJson = JSON.parse(ret[0]);
      cssJson = JSON.parse(ret[1]);
      console.log(`本地 lanaya-ui 版本为： ${jsJson.version}`);
      console.log(`本地 lanaya-ui-css 版本为： ${cssJson.version}`);
      if (jsJson.version === jsVersion && cssJson.version === cssVersion) {
        process.exit(0);
      } else {
        console.log('本地版本与npm最新版本不一致...');
        console.log('本地版本与npm最新版本不一致...');
        console.log('本地版本与npm最新版本不一致...');
        console.log('确认一下，是不是有谁往npm发布新版本之后，没有往git提交对应的package.json文件。');
        console.log('如果是的话，请把他打死！');
        process.exit(1);
      }

    } catch (e) {
      console.log('package.json文件格式不正确，看看是不是从git上拉取下来有冲突...');
      process.exit(1);
    }

  }).catch(() => {
    console.log('读取本地包版本信息失败...');
    process.exit(1);
  });
}
