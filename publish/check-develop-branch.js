const execSync = require('child_process').execSync;
console.log('向npm发布时，需要验证你的本地代码是否跟远程仓库的master分支保持一致。');
console.log('如果还没有连接VPN，请终止该操作...');

const branchInfo = `Your branch is behind 'origin/master'`;

let results;
try {
  results = execSync(`git fetch && git status`, {encoding: 'utf-8'});
} catch (e) {
  console.log('git命令有误，看看git是不是有什么问题了。');
  process.exit(1);
}

if (results) {
  if (results.includes(branchInfo)) {
    console.log('master分支代码落后于远程分支，请使用 git pull 更新代码');
    process.exit(1);
  } else {
    process.exit(0);
  }
  console.log('未知错误...');
  process.exit(1);
}

console.log('未知错误..');
process.exit(1);
