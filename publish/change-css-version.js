const path = require('path');
const {readPackageJsonFile, writePackageJsonFile, addVersion} = require('./utils');

const cssPackageJsonFile = path.resolve(__dirname, '../packages/styles/package.json');

readPackageJsonFile(cssPackageJsonFile).then(ret => {
  const cssJson = addVersion(ret);
  return writePackageJsonFile(cssPackageJsonFile, cssJson);
}).then(() => {
  console.log('change version complete');
});
