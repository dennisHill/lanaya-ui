const path = require('path');
const {readPackageJsonFile, writePackageJsonFile, addVersion} = require('./utils');

const jsPackageJsonFile = path.resolve(__dirname, '../package.json');

readPackageJsonFile(jsPackageJsonFile).then(ret => {
  const jsJson = addVersion(ret);
  return writePackageJsonFile(jsPackageJsonFile, jsJson);
}).then(() => {
  console.log('change version complete');
});
