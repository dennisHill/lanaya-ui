const execSync = require('child_process').execSync;

const branchInfo = '* master\n';

let results;
try {
  results = execSync(`git branch`, {encoding: 'utf-8'})
} catch (e) {
  console.log('git branch命令有误，看看git是不是有什么问题了。');
  process.exit(1);
}

if (results) {
  if (results.includes(branchInfo)) {
    process.exit(0);
  } else {
    console.log('当前未在master分支');
    process.exit(1);
  }
  console.log('未知错误...');
  process.exit(1);
}

console.log('未知错误..');
process.exit(1);
