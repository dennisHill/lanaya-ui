// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import lanayaUI from './../../src/index';
import './../../packages/styles/src/index.scss';
import './style/common.scss';

Vue.config.productionTip = false;
Vue.use(lanayaUI);

// 事件总线
Vue.prototype.$EventBus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
});
