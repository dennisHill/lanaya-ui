export default {
  example3: `
<ps-date v-model="dateModel" :format="format" :with-time="false"></ps-date>


export default {
  ...
  data() {
    return {
      dateModel: new Date(),
      format: 'yyyy-MM-dd'
    };
  }
  ...
}`,
  example2: `
<ps-date v-model="dateModel" :format="format"></ps-date>


export default {
  ...
  data() {
    return {
      dateModel: '2019-09-02 15:30:20',
      format: 'MM-dd HH:mm'
    };
  }
  ...
}`,
  example1: `
<ps-date v-model="dateModel"></ps-date>


export default {
  ...
  data() {
    return {
      dateModel: ''
    };
  }
  ...
}`
};
