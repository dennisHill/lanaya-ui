const example1 = `
<ps-button @click="showDialog">clickToShowDialog</ps-button>

<ps-dialog title="我是一个弹出框" :visible.sync="visible">
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <template v-slot:foot>
    <ps-button @click="closeDialog">关闭</ps-button>
  </template>
</ps-dialog>

export default {
  ...
  methods: {
    showDialog () {
      this.visible = true;
    },
    closeDialog () {
      this.visible = false;
    }
  },
  data () {
    return {
      visible: false
    };
  }
  ...
}

`;
const example2 = `
<ps-button @click="showDialog">clickToShowDialog</ps-button>

<ps-dialog title="我是一个弹出框" :visible.sync="visible" width="800px">
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <template v-slot:foot>
    <ps-button @click="closeDialog">关闭</ps-button>
  </template>
</ps-dialog>

export default {
  ...
  methods: {
    showDialog () {
      this.visible = true;
    },
    closeDialog () {
      this.visible = false;
    }
  },
  data () {
    return {
      visible: false
    };
  }
  ...
}

`;

const example3 = `
<ps-button @click="showDialog">clickToShowDialog</ps-button>

<ps-dialog title="我是一个弹出框" :visible.sync="visible" width="800px" :before-close="handleClose">
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <template v-slot:foot>
    <ps-button @click="closeDialog">关闭</ps-button>
  </template>
</ps-dialog>

export default {
  ...
  methods: {
    showDialog () {
      this.visible = true;
    },
    closeDialog () {
      this.visible = false;
    },
    handleClose (done) {
      alert(1);
      done();
    }
  },
  data () {
    return {
      visible: false
    };
  }
  ...
}

`;

const example4 = `
<ps-button @click="showDialog">clickToShowDialog</ps-button>

<ps-dialog title="我是一个弹出框" :visible.sync="visible" width="800px"
    @open="open"
    @close="close"
    @opened="opened"
    @closed="closed">
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <p>我是一个弹出框</p>
  <template v-slot:foot>
    <ps-button @click="closeDialog">关闭</ps-button>
  </template>
</ps-dialog>

export default {
  ...
  methods: {
    showDialog () {
      this.visible = true;
    },
    closeDialog () {
      this.visible = false;
    },
    open () {
      alert('open');
    },
    opened () {
      alert('opened');
    },
    close () {
      alert('close');
    },
    closed () {
      alert('closed');
    }
  },
  data () {
    return {
      visible: false
    };
  }
  ...
}

`;
export default {
  example1, example2, example3, example4
};
