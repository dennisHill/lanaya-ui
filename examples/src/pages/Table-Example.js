const example1 = `
<ps-table :table='table'></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }]
      })
    }
  }
  ...
}`;
const example2 = `
<ps-table :table='table'></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }],
        showSelect: true
      })
    }
  }
  ...
}`;
const example3 = `
<ps-table :table='table'></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }],
        buttons: [{
          label: '操作按钮1',
          click () {
            alert('点击了按钮1');
          }
        }, {
          label: '操作按钮2',
          color: 'red',
          click (row) {
            alert('当前行数据是\\n' + JSON.stringify(row));
          }
        }, {
          label: '操作按钮3',
          color: 'purple',
          outline: true,
          click (row, table) {
            alert('当前表格选中的数据是\\n' + JSON.stringify(table.getSelected()));
          }
        }],
        showSelect: true
      })
    }
  }
  ...
}
`;
const example4 = `
<ps-table :table='table'>
  <template v-slot:username='{ row }'>
    <ps-button>{{row.username}}</ps-button>
  </template>
  <template v-slot:phone='{ row }'>
    <ps-input v-model='row.phone'></ps-input>
  </template>
</ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }]
      })
    }
  }
  ...
}`;
const example5 = `
<ps-button @click="changeTableData" style="margin-bottom: 5px;">改变数据</ps-button>
<ps-table :table="table5"></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  methods: {
    changeTableData () {
      this.table.setData([{
        username: '11111',
        name: '科比 布莱恩特',
        age: 40,
        phone: '13800000000'
      }, {
        username: '22222',
        name: '勒布朗 詹姆斯',
        age: 33,
        phone: '13800000000'
      }]);
    }
  },
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }]
      })
    }
  }
  ...
}`;

const example6 = `
<ps-table :table="table"></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
        columns: [{
          label: '用户名',
          key: 'username'
        }, {
          label: '姓名',
          key: 'name'
        }, {
          label: '年龄',
          key: 'age'
        }, {
          label: '电话',
          key: 'phone'
        }],
        data: [{
          username: '1030006',
          name: 'David',
          age: 40,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Amy',
          age: 33,
          phone: '13800000000'
        }, {
          username: '1030004',
          name: 'Tom',
          age: 33,
          phone: '13800000000'
        }, {
          username: '2330004',
          name: 'Jack',
          age: 23,
          phone: '13800000000'
        }],
        showPage: false
      })
    }
  }
  ...
}`;
const example7 = `
<ps-table :table="table"></ps-table>

// 该组件接收一个table参数，这个参数类型是 PsUI.Table，所以，这里需要再次引入 proudsmart-ui
import PsUI from 'proudsmart-ui';
import axios from 'axios';

export default {
  ...
  data() {
    return {
      table: new PsUI.Table({
      ajax (page) {
        return axios.post('api/rest/post/alertQueryFlexService/getAlertByPage', [{
          severities: '1,2,3,4',
          states: '0,5,10,20,30',
          domain: '/0/515445641576227/'
        }, {
          start: (page.pageNumber - 1) * page.pageSize,
          length: page.pageSize,
          statCount: true
        }]).then(response => {
          let {data} = response;
            if (data.code === 0 || data.code === '0') {
              return data.data;
            } else {
              return data;
            }
          });
        },
        realPage: true,
        columns: [{
          key: 'alertId',
          label: '报警编号'
        }, {
          key: 'severity',
          label: '报警级别'
        }, {
          key: 'firstArisingTime',
          label: '首次报警时间',
          type: 'dateTime'
        }, {
          key: 'count',
          label: '报警次数'
        }, {
          key: 'arisingTime',
          label: '最近报警时间',
          type: 'dateTime'
        }, {
          key: 'instanceName',
          label: '报警位置'
        }, {
          key: 'appName',
          label: '报警来源'
        }, {
          key: 'message',
          label: '报警描述',
          edit: true
        }, {
          key: 'state',
          label: '报警状态'
        }]
      })
    }
  }
  ...
}`;
export default {
  example7,
  example6: example6,
  example5: example5,
  example4: example4,
  example3: example3,
  example2: example2,
  example1: example1
};
