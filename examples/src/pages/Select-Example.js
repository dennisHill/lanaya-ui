const example1 = `
<!-- 宽度默认为100%，会充满父容器的宽度 -->
<ps-select v-model="selectMode" :options="options" placeholder="选个饭店吃饭吧" width="200px"></ps-select>

export default {
  ...
  data() {
    return {
      selectMode: '',
      options: [{
        id: 1,
        label: '沙县小吃'
      }, {
        id: 2,
        label: '兰州拉面'
      }, {
        id: 3,
        label: '新疆大盘鸡'
      }]
    }
  }
  ...
}`;

const example2 = `
<p>以label作为model的值：{{selectModel1}}</p>
<ps-select v-model="selectModel1" :options="options" :format="format" placeholder="选个饭店吃饭吧" width="200px"></ps-select>

<p>默认以id作为model的值：{{selectModel2}}</p>
<ps-select v-model="selectModel2" :options="options" placeholder="选个饭店吃饭吧" width="200px"></ps-select>

export default {
  ...
  data() {
    return {
      selectMode1: '',
      selectModel2: '',
      options: [{
        id: 1,
        label: '沙县小吃'
      }, {
        id: 2,
        label: '兰州拉面'
      }, {
        id: 3,
        label: '新疆大盘鸡'
      }],
      format: {
        label: 'label',
        value: 'label'
      }
    }
  }
  ...
}`;

const example3 = `
<!-- 宽度默认为100%，会充满父容器的宽度 -->
<ps-select v-model="selectMode" :options="options" :filter="false" width="200px"></ps-select>

export default {
  ...
  data() {
    return {
      selectMode: '',
      options: [{
        id: 1,
        label: '沙县小吃'
      }, {
        id: 2,
        label: '兰州拉面'
      }, {
        id: 3,
        label: '新疆大盘鸡'
      }]
    }
  }
  ...
}`;

const example4 = `
<!-- 宽度默认为100%，会充满父容器的宽度 -->
<ps-select v-model="selectMode" :options="options" @change="changeSelect" width="200px"></ps-select>

export default {
  ...
  data() {
    return {
      selectMode: '',
      options: [{
        id: 1,
        label: '沙县小吃'
      }, {
        id: 2,
        label: '兰州拉面'
      }, {
        id: 3,
        label: '新疆大盘鸡'
      }]
    }
  },
  methods: {
    changeSelect (model, option) {
      alert('model值为：' + model);
      alert('选择的选项为：' + JSON.stringify(option));
    }
  }
  ...
}`;

export default {
  example1: example1,
  example2: example2,
  example3: example3,
  example4: example4
};
