const example1 = `
<l-button>按钮</l-button>
`;

const example2 = `
<l-button>主要按钮</l-button>
<l-button status="info">Info按钮</l-button>
<l-button status="success">成功按钮</l-button>
<l-button status="danger">危险按钮</l-button>
<l-button status="warning">警告按钮</l-button>
`;

const example3 = `
<l-button size="xsmall">超小号</l-button>
<l-button size="small">小号</l-button>
<l-button>默认尺寸</l-button>
<l-button size="large">大号</l-button>
<l-button size="xlarge">超大号</l-button>
`;

const example4 = `
<l-button outline>有外框</l-button>
<l-button>默认无外框</l-button>`;

const example5 = `
<l-button outline @click="clickButton">狠狠的点我</l-button>

export default {
  ...
  methods: 
    clickButton() {
      alert('让你点，你就点？');
    }
  }
  ...
}`;

const example6 = `
<l-button-group>
  <l-button outline status="primary" @click="clickButton">狠狠的点我</l-button>
  <l-button outline status="danger" @click="clickButton">狠狠的点我</l-button>
  <l-button outline status="info" @click="clickButton">狠狠的点我</l-button>
  <l-button outline status="success" @click="clickButton">狠狠的点我</l-button>
  <l-button outline status="warning" @click="clickButton">狠狠的点我</l-button>
</l-button-group>
<l-button-group style="margin-top: 15px;">
  <l-button status="primary" @click="clickButton">狠狠的点我</l-button>
  <l-button status="danger" @click="clickButton">狠狠的点我</l-button>
  <l-button status="info" @click="clickButton">狠狠的点我</l-button>
  <l-button status="success" @click="clickButton">狠狠的点我</l-button>
  <l-button status="warning" @click="clickButton">狠狠的点我</l-button>
</l-button-group>`;

export default {
  example1: example1,
  example2: example2,
  example3: example3,
  example4: example4,
  example5: example5,
  example6: example6
};
