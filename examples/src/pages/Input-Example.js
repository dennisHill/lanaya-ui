const example1 = `
<!-- 默认情况下，ps-input会充满父容器的宽度 -->
<ps-input v-model="inputModel" width="200px" placeholder="我想不到写点什么了"></ps-input>

export default {
  ...
  data() {
    return {
      inputModel: ''
    };
  }
  ...
}`;

const example2 = `
<ps-input v-model="inputModel1" width="200px" type="number"></ps-input>

<ps-input v-model="inputModel2" width="200px" type="password"></ps-input>

export default {
  ...
  data() {
    return {
      inputModel1: '',
      inputModel2: ''
    };
  }
  ...
}
`;

export default {
  example1: example1,
  example2: example2
};
