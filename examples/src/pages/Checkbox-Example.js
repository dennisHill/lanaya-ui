const example1 = `
<ps-checkbox v-model="checkboxModel">复选框</ps-checkbox>

export default {
  ...
  data () {
    return {
      checkboxModel: false
    };
  }
  ...
};
`;

const example2 = `
<ps-checkbox v-model="checkboxModel" @change="checkboxChange">复选框</ps-checkbox>

export default {
  ...
  data () {
    return {
      checkboxModel: true
    };
  },
  methods: {
    checkboxChange (model) {
      alert('现在复选框的model值为：' + model);
    }
  }
  ...
};
`;

export default {
  example1: example1,
  example2: example2
};
