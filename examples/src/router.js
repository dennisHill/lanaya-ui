import Vue from 'vue';
import Router from 'vue-router';
import Home from './pages/Home';
import Table from './pages/Table-E';
import Input from './pages/Input-E';
import Date from './pages/Date-E';
import Select from './pages/Select-E';
import Button from './pages/Button-E';
import Checkbox from './pages/Checkbox-E';
import Dialog from './pages/Dialog-E';
import Status from './pages/Status-E';

Vue.use(Router);

export default new Router({
  routes: [{
    path: '/',
    name: 'Home',
    component: Home
  }, {
    path: '/table',
    name: 'Table',
    component: Table
  }, {
    path: '/input',
    name: 'Input',
    component: Input
  }, {
    path: '/date',
    name: 'Date',
    component: Date
  }, {
    path: '/select',
    name: 'Select',
    component: Select
  }, {
    path: '/button',
    name: 'Button',
    component: Button
  }, {
    path: '/checkbox',
    name: 'Checkbox',
    component: Checkbox
  }, {
    path: '/dialog',
    name: 'Dialog',
    component: Dialog
  }, {
    path: '/status',
    name: 'Status',
    component: Status
  }]
});
