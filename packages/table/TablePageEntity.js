class Page {
  constructor (page) {
    // 每页条数
    this.pageSize = page.pageSize || 10;
    // 当前页码
    this.pageNumber = 0;
    // 总条数
    this.recordTotal = page.total;
    // 总页数
    this.totalPage = Math.ceil(this.recordTotal / this.pageSize);
    // 所有的页码
    this.allPageNumber = [...new Array(this.totalPage).keys()];
    // 所有应显示的页码
    this.allShowedPageNumber = [];
    this.setShowedPageNumber();
  }

  setTotal (total) {
    this.recordTotal = total;
    this.totalPage = Math.ceil(this.recordTotal / this.pageSize);
    this.allPageNumber = [...new Array(this.totalPage).keys()];
    this.setShowedPageNumber();
  }

  setShowedPageNumber () {
    if (this.totalPage < 6) {
      this.allShowedPageNumber = this.allPageNumber;
    }
    if (this.totalPage > 5) {
      if (this.pageNumber < 3) {
        this.allShowedPageNumber = this.allPageNumber.slice(0, 5);
      } else if (this.allPageNumber[this.totalPage - 1] - this.pageNumber < 3) {
        this.allShowedPageNumber = this.allPageNumber.slice(this.totalPage - 5);
      } else {
        this.allShowedPageNumber = this.allPageNumber.slice(this.pageNumber - 2, this.pageNumber + 3);
      }
    }
  }

  showBeforePageNumber () {
    if (this.allShowedPageNumber[0] < 5) {
      this.allShowedPageNumber = this.allPageNumber.slice(0, 5);
    } else {
      this.allShowedPageNumber = this.allShowedPageNumber.map(num => {
        return num - 5;
      });
    }
  }

  showAfterPageNumber () {
    if (this.totalPage - 1 - this.allShowedPageNumber[this.allShowedPageNumber.length - 1] < 5) {
      this.allShowedPageNumber = this.allPageNumber.slice(this.totalPage - 5);
    } else {
      this.allShowedPageNumber = this.allShowedPageNumber.map(num => {
        return num + 5;
      });
    }
  }

  prevPage () {
    if (this.pageNumber === 0) {
      return;
    }
    this.pageNumber--;
    this.setShowedPageNumber();
  }

  nextPage () {
    if (this.pageNumber === this.totalPage - 1) {
      return;
    }
    this.pageNumber++;
    this.setShowedPageNumber();
  }

  goToPage (p) {
    this.pageNumber = p;
    this.setShowedPageNumber();
  }

  getPageInfo () {
    return `第 ${this.recordTotal === 0 ? 0 : this.pageNumber * this.pageSize + 1} 至 ${(this.pageNumber + 1) * this.pageSize >= this.recordTotal ? this.recordTotal : (this.pageNumber + 1) * this.pageSize} 项， 共 ${this.recordTotal} 项`;
  }

  changePageSize (size) {
    this.pageSize = size;
    this.pageNumber = 0;
    this.totalPage = Math.ceil(this.recordTotal / this.pageSize);
    this.allPageNumber = [...new Array(this.totalPage).keys()];
    this.setShowedPageNumber();
  }
}

export default Page;
