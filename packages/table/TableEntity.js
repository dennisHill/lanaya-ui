import Vue from 'vue';
import Page from './TablePageEntity';
import { attr } from './../../src/utils/utils';

class Table {
  constructor (props) {
    this.init(props);
  }

  init (props) {
    this.columns = props.columns || [];
    this.data = props.data || [];
    this.url = props.url;
    this.showSelect = !!props.showSelect;
    this.showPage = typeof props.showPage === 'undefined' ? true : !!props.showPage;
    this.allSelected = false;
    this.operateButtons = props.buttons || [];
    this.showLoading = false;
    this.page = new Page({
      pageSize: props.pageSize,
      total: props.total || this.data.length
    });
    this.inlineEdit = !!props.inlineEdit;
    this.inlineAdd = false;
    this.inlineAddRow = {};
    this.realPage = !!props.realPage;
    this.ajax = props.ajax;
    this.initToExecuteAjax = typeof props.initToExecuteAjax === 'undefined' ? true : !!props.initToExecuteAjax;
    if (this.initToExecuteAjax) {
      this.request();
    }
    this.initData();
  }

  refresh (parameter) {
    if (this.ajax && typeof this.ajax === 'function') {
      this.showLoading = true;
      this.ajax({
        page: {
          pageSize: this.page.pageSize,
          pageNumber: this.page.pageNumber + 1
        },
        parameter
      }).then(ret => {
        this.showLoading = false;
        if (this.realPage) {
          this.setData(ret.data, ret.total);
        } else {
          this.setData(ret);
        }
      }).catch(e => {
        this.showLoading = false;
        console.error(e);
      });
    }
  }

  request () {
    if (this.ajax && typeof this.ajax === 'function') {
      this.showLoading = true;
      this.ajax({
        page: {
          pageSize: this.page.pageSize,
          pageNumber: this.page.pageNumber + 1
        }
      }).then(ret => {
        this.showLoading = false;
        if (this.realPage) {
          this.setData(ret.data, ret.total);
        } else {
          this.setData(ret);
        }
      }).catch(e => {
        this.showLoading = false;
        console.error(e);
      });
    }
  }

  beginInlineAdd () {
    if (this.inlineAdd) {
      return;
    }
    this.inlineAddRow = {};
    this.inlineAdd = true;
  }

  finishInlineAdd () {
    this.inlineAdd = false;
  }

  addRow (row, isLast) {
    let d = [...this.data];
    Vue.set(row, 'selected', false);
    Vue.set(row, 'edit', false);
    if (isLast) {
      d.push(row);
    } else {
      d.unshift(row);
    }
    this.setData(d, this.page.recordTotal + 1);
  }

  setTempEditRowData (row) {
    let obj = {};
    this.columns.forEach(column => {
      obj[column.key] = attr(row, column.key);
    });
    Vue.set(row, 'tempEditData', obj);
  }

  setEditRowData (row) {
    for (let key in row.tempEditData) {
      attr(row, key, row.tempEditData[key]);
    }
  }

  initData () {
    this.data.forEach(item => {
      Vue.set(item, 'selected', false);
      Vue.set(item, 'edit', false);
    });
  }

  setAllSelected (value) {
    this.data.forEach(item => {
      item.selected = value;
    });
  }

  selectedChange () {
    let selectedData = this.data.filter(item => {
      return item.selected;
    });
    if (selectedData.length === this.data.length) {
      this.allSelected = true;
    } else {
      this.allSelected = false;
    }
  }

  getSelected () {
    return this.data.filter(item => {
      return item.selected;
    });
  }

  getCurrentPageNumber () {
    return this.page.pageNumber;
  }

  getAllPageNumber () {
    return this.page.allShowedPageNumber;
  }

  getMaxPageNumber () {
    return this.page.totalPage - 1;
  }

  showBeforePageNumber () {
    this.page.showBeforePageNumber();
  }

  showAfterPageNumber () {
    this.page.showAfterPageNumber();
  }

  prevPage () {
    this.page.prevPage();
    this.realPage ? this.request() : this.getCurrentData();
  }

  nextPage () {
    this.page.nextPage();
    this.realPage ? this.request() : this.getCurrentData();
  }

  goToPage (p) {
    this.page.goToPage(p);
    this.realPage ? this.request() : this.getCurrentData();
  }

  goToFirstPage () {
    this.goToPage(0);
    this.realPage ? this.request() : this.getCurrentData();
  }

  goToLastPage () {
    this.goToPage(this.page.totalPage - 1);
    this.getCurrentData();
    this.realPage ? this.request() : this.getCurrentData();
  }

  getPageInfo () {
    return this.page.getPageInfo();
  }

  changePageSize (size) {
    this.page.changePageSize(size);
    this.page.pageNumber = 0;
    this.realPage ? this.request() : this.getCurrentData();
  }

  getCurrentData () {
    if (this.showPage && !this.realPage) {
      let beginIndex = this.page.pageNumber * this.page.pageSize;
      let endIndex = (this.page.pageNumber + 1) * this.page.pageSize - 1;
      return this.data.slice(beginIndex, endIndex + 1);
    }
    return this.data;
  }

  setData (d = [], total) {
    this.data = d;
    this.initData();
    total = this.realPage ? total : this.data.length;
    this.page.setTotal(total);
    this.getCurrentData();
  }
}

export default Table;
