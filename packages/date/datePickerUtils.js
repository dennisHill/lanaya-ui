import Vue from 'vue';
import DatePicker from './DatePicker';
import { pop, transitionEndEventListener } from './../../src/utils/utils';

const DatePickerUtils = {
  datePickerComponent: null,
  destroyPicker () {
    document.removeEventListener('click', document.clickHandlers);
    this.datePickerComponent.$el.style.opacity = 0;
    this.datePickerComponent.$destroy();
    let el = this.datePickerComponent.$el;
    transitionEndEventListener(el);
    this.datePickerComponent = null;
  },
  createPicker (prop, vm) {
    if (this.datePickerComponent) {
      this.destroyPicker();
    }
    let Picker = Vue.extend(DatePicker);
    let datePickerComponent = pop(Picker, prop, vm);
    this.datePickerComponent = datePickerComponent;

    // 放在nextTick中执行，否则，计算datePickerDom的高度时有问题
    datePickerComponent.$nextTick(() => {
      let datePickerDom = datePickerComponent.$el;
      let datePickerDomRectObject = datePickerDom.getBoundingClientRect();

      let dateDomRectObject = vm.$el.getBoundingClientRect();
      if (window.innerWidth - dateDomRectObject.left < 400) {
        datePickerDom.style.right = window.innerWidth - dateDomRectObject.width - dateDomRectObject.left + 'px';
      } else {
        datePickerDom.style.left = dateDomRectObject.left + 'px';
      }
      if (window.innerHeight - dateDomRectObject.height - dateDomRectObject.top < 450) {
        datePickerDom.style.top = (dateDomRectObject.top - datePickerDomRectObject.height - 3) + 'px';
      } else {
        datePickerDom.style.top = (dateDomRectObject.top + dateDomRectObject.height + 2) + 'px';
      }
      setTimeout(() => {
        datePickerDom.style.opacity = 1;
      });
    });
  }
};

export { DatePickerUtils };
