// 加载gulp
const gulp = require('gulp');
// 读取用于编译Sass的插件
const sass = require('gulp-sass');
// 压缩css
const cssmin = require('gulp-cssmin');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const rename = require('gulp-rename');

let processer = [autoprefixer({
  overrideBrowserslist: ['> 1%', 'last 5 versions']
})];

gulp.task('css', function () {
  return gulp.src('./src/*.scss')
    .pipe(sass())
    .pipe(postcss(processer))
    .pipe(gulp.dest('./lib/lanaya-ui-css'))
    .pipe(cssmin())
    .pipe(rename({extname: '.min.css'}))
    .pipe(gulp.dest('./lib/lanaya-ui-css'));
});

gulp.task('iconfont', function () {
  return gulp.src('./src/fonts/*')
    .pipe(gulp.dest('./lib/lanaya-ui-css/fonts'));
});

gulp.task('build', gulp.series('css', 'iconfont'));
