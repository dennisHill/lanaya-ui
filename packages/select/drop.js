import Vue from 'vue';
import SelectDrop from './SelectDrop';
import MSelectDrop from './MSelectDrop';
import { pop, transitionEndEventListener } from './../../src/utils/utils';

const shouldHiddenWhenClick = (el) => {
  while (el !== document.body) {
    if (el.getAttribute('click-not-hidden') === 'true') {
      return false;
    }
    el = el.parentNode;
  }
  return true;
};

const DropList = {
  dropComponent: null,
  destroyDrop () {
    document.removeEventListener('click', document.clickHandler);
    document.removeEventListener('scroll', document.scrollHandler);
    this.dropComponent.$destroy();
    this.dropComponent.$el.style.height = 0;
    transitionEndEventListener(this.dropComponent.$el);
  },
  createDrop (prop, vm) {
    let Drop = Vue.extend(SelectDrop);
    // 手动挂载drop list组件
    let dropComponent = pop(Drop, prop, vm);
    this.dropComponent = dropComponent;
    let dropDom = dropComponent.$el;
    let dropDomRectObject = dropDom.getBoundingClientRect();
    let dropDomHeight = dropDomRectObject.height;

    let selectDom = vm.$el;
    let selectDomRectObject = selectDom.getBoundingClientRect();
    let selectDomHeight = selectDomRectObject.height;
    let selectDomTop = selectDomRectObject.top;

    dropDom.style.width = selectDomRectObject.width + 'px';
    dropDom.style.left = selectDomRectObject.left + 'px';

    // 计算下拉框位置，如果比较靠下的话，就在上方显示
    if (window.innerHeight - selectDomHeight - selectDomTop < 150) {
      dropDom.style.top = (selectDomRectObject.top - dropDomRectObject.height - 10) + 'px';
    } else {
      dropDom.style.top = (selectDomRectObject.top + selectDomRectObject.height + 2) + 'px';
    }
    dropDom.style.height = 0;
    setTimeout(() => {
      dropDom.style.height = dropDomHeight + 'px';
    });
    /**
     * 利用document代理click事件，如果点击的不是选择框，则收起下拉框
     */
    document.addEventListener('click', function clickHandler (e) {
      document.clickHandler = clickHandler;
      if (!vm.$el.contains(e.target) && shouldHiddenWhenClick(e.target)) {
        vm.isDrop = false;
        document.removeEventListener('click', document.clickHandler);
        dropComponent.$destroy();
        dropDom.style.height = 0;
        transitionEndEventListener(dropDom);
      }
    });
    document.addEventListener('scroll', function scrollHandler (e) {
      document.scrollHandler = scrollHandler;
      if (!dropDom.contains(e.target)) {
        document.removeEventListener('scroll', document.scrollHandler);
        vm.isDrop = false;
        dropComponent.$destroy();
        dropDom.remove();
      }
    }, true);
  }
};

const MDropList = {
  dropComponent: null,
  destroyDrop () {
    document.removeEventListener('click', document.clickHandlers);
    this.dropComponent.$destroy();
    this.dropComponent.$el.remove();
  },
  createDrop (prop, vm) {
    let Drop = Vue.extend(MSelectDrop);
    let dropComponent = pop(Drop, prop, vm);
    this.dropComponent = dropComponent;
    let dropDom = dropComponent.$el;
    let dropBoxDom = dropDom.children[0];
    dropBoxDom.style.transform = 'scale(0.8)';
    dropBoxDom.style.opacity = 0;
    setTimeout(() => {
      dropBoxDom.style.transform = 'scale(1)';
      dropBoxDom.style.opacity = 1;
    });
    document.addEventListener('click', function clickHander (e) {
      document.clickHander = clickHander;
      if (!vm.$el.contains(e.target)) {
        vm.isDrop = false;
        document.removeEventListener('click', document.clickHander);
        dropComponent.$destroy();
        dropDom.remove();
      }
    });
  }
};

export { DropList, MDropList };
