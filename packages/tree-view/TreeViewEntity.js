import {convertArrayToTreeArray, deleteItemFromTreeArray} from './../../src/utils/utils';

class TreeView {
  constructor (opt) {
    this.init(opt);
  }
  init (opt) {
    this.formatStyle(opt.format);
    this.originalData = opt.data || [];
    this.buttons = opt.buttons || [];
    this.setButtons();
    this.data = convertArrayToTreeArray(this.originalData, this.format);
  }
  setButtons () {
    this.originalData.forEach(item => {
      item.buttons = this.buttons;
    });
  }
  setNoChildren () {
    this.originalData.forEach(item => {
      item.children = [];
    });
  }
  formatStyle (format = {}) {
    this.format = {
      id: format.id || 'id',
      label: format.label || 'label',
      parent: format.parent || 'parent'
    };
  }
  setData (data) {
    data.forEach(item => {
      item.children = item.children || [];
      item._expand = true;
    });
    this.originalData = data || [];
    this.setButtons();
    this.data = convertArrayToTreeArray(this.originalData, this.format);
  }
  addItem (item) {
    item.children = item.children || [];
    item._expand = true;
    this.originalData.push(item);
    this.setNoChildren();
    this.setButtons();
    this.data = convertArrayToTreeArray(this.originalData, this.format);
  }
  deleteItem (item) {
    this.originalData = deleteItemFromTreeArray(this.originalData, item, this.format);
    this.setNoChildren();
    this.setButtons();
    this.data = convertArrayToTreeArray(this.originalData, this.format);
  }
}

export default TreeView;
