import Input from './../packages/input/Input';
import Button from './../packages/button/Button';
import ButtonGroup from '../packages/button/ButtonGroup';
import Checkbox from './../packages/checkbox/Checkbox';
import Select from './../packages/select/Select';
import TreeView from '../packages/tree-view/TreeView';
import TreeViewEntity from '../packages/tree-view/TreeViewEntity';
import Dialog from '../packages/dialog/Dialog';
import Table from '../packages/table/Table';
import TableEntity from '../packages/table/TableEntity';
import Date from '../packages/date/Date';

const install = function (Vue) {
  if (install.installed) {
    return;
  }

  Vue.component(Input.name, Input);
  Vue.component(Button.name, Button);
  Vue.component(ButtonGroup.name, ButtonGroup);
  Vue.component(Checkbox.name, Checkbox);
  Vue.component(Select.name, Select);
  Vue.component(TreeView.name, TreeView);
  Vue.component(Dialog.name, Dialog);
  Vue.component(Table.name, Table);
  Vue.component(Date.name, Date);
};

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export { Input, Button, ButtonGroup, Checkbox, Select, TreeView, Dialog, Table, Date };

export default {
  install: install,
  TreeView: TreeViewEntity,
  Table: TableEntity
};
